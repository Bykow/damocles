# Longhorn

This provides storage to pod through PV/PVC.

## Installation Requirements

[Longhorn documentation](https://longhorn.io/docs/1.6.1/deploy/install/#installation-requirements)
You can use the `extra-install.yml` playbook in the kubespray-config repo to ease the installation.

## Node Label

```
kubectl label nodes k8s-worker-0 "node.longhorn.io/create-default-disk=true"
kubectl label nodes k8s-worker-1 "node.longhorn.io/create-default-disk=true"
kubectl label nodes k8s-worker-2 "node.longhorn.io/create-default-disk=true"
```

## Node configuration

Once longhorn is installed, you need to access the webUI and configure the nodes.

## Stuck namespace deletion
```
(
NAMESPACE=longhorn-system
kubectl proxy &
kubectl get namespace $NAMESPACE -o json |jq '.spec = {"finalizers":[]}' >temp.json
curl -k -H "Content-Type: application/json" -X PUT --data-binary @temp.json 127.0.0.1:8001/api/v1/namespaces/$NAMESPACE/finalize
)
```
