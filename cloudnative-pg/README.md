# CNPG

## Recover Strategy
See recovery.yaml

This is an example recovery cluster file that can be applied with `kubectl apply -f recovery.yaml`.

This will create a new cluster and download the backups into it.

Be mindfull of the name of the new cluster and eventually adapt the existing workloads.
