# Kubespray
This repo holds some configuration override for kubespray.

## cluster-config.yml
Contains some vars override for the cluster configuration and basic componenents.
- enable dual stack cluster (ipv4 ipv6)
- choses cilium as CNI
- add kube-vip for control plane address

## inventory.ini
Defines the nodes of the cluster.

## cluster upgrade
To upgrade the cluster, follow [kubespray's documentation](https://kubespray.io/#/docs/operations/nodes?id=_2-upgrade-the-cluster)


First follow the ansible setup:
https://kubespray.io/#/docs/ansible/ansible?id=installing-ansible

Then while being in the `kubespray-venv`
```
ANSIBLE_STDOUT_CALLBACK=yaml ansible-playbook -i ../k8s-1/1-cluster/kubespray-config/hosts.yml -e @../k8s-1/1-cluster/kubespray-config/cluster-config.yml --user=root --become --become-user=root upgrade-cluster.yml
```

The playbook takes about 15 minutes to complete.

## Initial install
Take some time to edit [cluster-config.yml](./cluster-config.yml)

Assuming the following directory structure:
```
|- k8s
   |- k8s-1
   |     |- applications
   |     |- argocd
   |     |- cilium
   |     |- external-secrets
   |     |- kubespray-config
   |- kubespray
```

ONE TIME:
```
cd k8s
pushd kubespray
VENVDIR=kubespray-venv
KUBESPRAYDIR=kubespray
python3 -m venv $VENVDIR
source $VENVDIR/bin/activate
cd $KUBESPRAYDIR
pip install -U -r requirements.txt
popd
```

Then:
```
cd k8s

pushd kubespray
source kubespray-venv/bin/activate
ANSIBLE_STDOUT_CALLBACK=yaml ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i ../k8s-1/1-cluster/kubespray-config/inventory.ini -e @../k8s-1/1-cluster/kubespray-config/cluster-config.yml --user=root --become --become-user=root cluster.yml
popd

# Few edition to make here
pushd k8s-1
# scp root@k8s-1-master-000f2-enaov.nymtech.cc:/root/.kube/config ~/.kube/k8s-1.conf && sed -i -e 's/127.0.0.1/cp.k8s-1.nymtech.cc/g' ~/.kube/k8s-1.conf
# ANSIBLE_STDOUT_CALLBACK=yaml ansible-playbook -i 1-cluster/kubespray-config/inventory.ini --user=root --become --become-user=root 1-cluster/kubespray-config/ansible-extra.yml
# echo 'alias k1="KUBECONFIG=~/.kube/k8s-1.conf kubectl"' >> ~/.bashrc     <== OR ELSE CHECK THIS /!\
k1 create namespace external-secrets
popd

=== Once again after external-secrets ===
pushd k8s-1/cilium
k1 kustomize --enable-helm --helm-kube-version=1.31 | k1 apply -f -
k1 apply -f lbpool.yaml
popd

# k1 apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.7.1/deploy/longhorn.yaml

=== THREE TIMES (CRDs) ===
pushd k8s-1/external-secrets
k1 apply -f stores/bitwarden/vaultwarden-credentials.yaml
k1 kustomize --enable-helm --helm-kube-version=1.31 | k1 apply -f -
popd

```
